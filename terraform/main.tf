provider "aws" {
  region = "us-west-2"
}

# Unencrypted S3 Bucket (vulnerable example)
resource "aws_s3_bucket" "example_bucket" {
  bucket = "example-vulnerable-bucket"

  acl    = "public-read"
}

# Insecure Security Group (vulnerable example)
resource "aws_security_group" "example_sg" {
  name        = "example-insecure-sg"
  description = "Allow all inbound traffic on port 22"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Overly permissive IAM Policy (vulnerable example)
resource "aws_iam_policy" "example_iam_policy" {
  name        = "example-insecure-policy"
  description = "Policy with overly broad permissions"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action   = "*",  # Allows all actions
        Effect   = "Allow",
        Resource = "*",  # Applies to all resources
      }
    ]
  })
}
