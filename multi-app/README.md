# Multi App

## Initialize

```shell
yarn
```

## Start

```shell
yarn start:multi-app-1
```

## Build

```shell
yarn build:multi-app-1
```
