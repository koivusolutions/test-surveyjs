import SurveyCard from "@test-surveyjs/shared/src/survey/SurveyCard"
import React from "react"

const surveyModel = {
    "pages": [
        {
            "name": "page1",
            "elements": [
                {
                    "type": "checkbox",
                    "name": "multiApp2",
                    "title": "Multi App 2 Question",
                    "choices": [
                        "Multi 2 A",
                        "Multi 2 B",
                        "Multi 2 C"
                    ]
                }
            ]
        }
    ]
};

const data: any[] = [
    {
        "multiApp2": [
            "Multi 2 A"
        ]
    },
    {
        "multiApp2": [
            "Multi 2 A"
        ]
    },
    {
        "multiApp2": [
            "Multi 2 B"
        ]
    },
];

const App: React.FunctionComponent = () => {
    return (
        <SurveyCard surveyModel={surveyModel} data={data} />
    );
}

export default App;
