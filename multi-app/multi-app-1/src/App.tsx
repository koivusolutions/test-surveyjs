import SurveyCard from "@test-surveyjs/shared/src/survey/SurveyCard"
import React from "react"

const surveyModel = {
    "pages": [
        {
            "name": "page1",
            "elements": [
                {
                    "type": "checkbox",
                    "name": "multiApp1",
                    "title": "Multi App 1 Question",
                    "choices": [
                        "Multi 1 A",
                        "Multi 1 B",
                        "Multi 1 C"
                    ]
                }
            ]
        }
    ]
};

const data: any[] = [
    {
        "multiApp1": [
            "Multi 1 A"
        ]
    },
    {
        "multiApp1": [
            "Multi 1 A"
        ]
    },
    {
        "multiApp1": [
            "Multi 1 B"
        ]
    },
];

const App: React.FunctionComponent = () => {
    return (
        <SurveyCard surveyModel={surveyModel} data={data} />
    );
}

export default App;
