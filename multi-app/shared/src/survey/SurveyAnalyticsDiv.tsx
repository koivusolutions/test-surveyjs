import React from "react"
import "survey-analytics/survey.analytics.css"
import { Model, StylesManager } from "survey-react"
import "survey-react/modern.css"

interface IProps {
    surveyModel: any;
    data: any[];
}

const SurveyAnalyticsDiv: React.FunctionComponent<IProps> = (props) => {

    React.useEffect(() => {
        import("survey-analytics").then(({ VisualizationPanel }) => {
            StylesManager.applyTheme("modern");
            const model = new Model(props.surveyModel);
            const panel = new VisualizationPanel(model.getAllQuestions(), props.data, {
                allowDynamicLayout: false,
                allowHideQuestions: false
            });
            panel.showHeader = false;
            const formSummaryDiv = document.getElementById("analyticsDiv");
            if (formSummaryDiv) {
                panel.render(formSummaryDiv);
            }
        }).catch((error) => {
            console.error(error);
        });
    }, [props.surveyModel, props.data]);

    return <div id="analyticsDiv" />;
}

export default SurveyAnalyticsDiv;
