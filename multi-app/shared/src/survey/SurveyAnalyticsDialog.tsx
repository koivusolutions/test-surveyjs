import {
    Button,
    Dialog,
    DialogActions,
    DialogContent
    } from "@material-ui/core"
import React from "react"
import SurveyAnalyticsDiv from "./SurveyAnalyticsDiv"

interface IProps {
    surveyModel: any;
    data: any[];
    onClose: () => void;
}

const SurveyAnalyticsDialog: React.FunctionComponent<IProps> = (props) => {
    return (
        <Dialog open={true} maxWidth="lg" fullWidth={true}>
            <DialogContent>
                <SurveyAnalyticsDiv surveyModel={props.surveyModel} data={props.data} />
            </DialogContent>
            <DialogActions>
                <Button variant="contained" onClick={props.onClose}>Close</Button>
            </DialogActions>
        </Dialog>
    );
}

export default SurveyAnalyticsDialog;
