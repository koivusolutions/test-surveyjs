import {
    Button,
    Dialog,
    DialogActions,
    DialogContent
    } from "@material-ui/core"
import React from "react"
import SurveyCreatorDiv from "./SurveyCreatorDiv"

interface IProps {
    surveyModel: any;
    onClose: () => void;
}

const SurveyCreatorDialog: React.FunctionComponent<IProps> = (props) => {
    return (
        <Dialog open={true} maxWidth="lg" fullWidth={true}>
            <DialogContent>
                <SurveyCreatorDiv surveyModel={props.surveyModel} />
            </DialogContent>
            <DialogActions>
                <Button variant="contained" onClick={props.onClose}>Close</Button>
            </DialogActions>
        </Dialog>
    );
}

export default SurveyCreatorDialog;
