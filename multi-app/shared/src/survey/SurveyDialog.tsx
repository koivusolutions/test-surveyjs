import {
    Button,
    Dialog,
    DialogActions,
    DialogContent
    } from "@material-ui/core"
import React from "react"
import { Model, StylesManager, Survey } from "survey-react"
import "survey-react/modern.css"
import "survey-react/survey.css"

interface IProps {
    surveyModel: any;
    onClose: () => void;
}
const SurveyDialog: React.FunctionComponent<IProps> = (props) => {
    const [model, setModel] = React.useState<any | null>();
    const [formResponseObject, setFormResponseObject] = React.useState<any>();

    React.useEffect(() => {
        StylesManager.applyTheme("modern");
        const model = new Model(props.surveyModel);
        setModel(model);
    }, [props.surveyModel]);

    const onComplete = (formResponseObject: any) => {
        setFormResponseObject(formResponseObject.data);
    }

    const onDownloadPdf = () => {
        import("survey-pdf")
            .then(({ SurveyPDF }) => {
                const options: any = {
                    fontSize: 14,
                    margins: {
                        left: 10,
                        right: 10,
                        top: 10,
                        bot: 10,
                    },
                    orientation: "p",
                    format: [210, 297],
                };
                const pdf = new SurveyPDF(props.surveyModel, options);
                pdf.data = formResponseObject;
                pdf.save("survey.pdf");
            })
            .catch((error) => {
                console.error(error);
            });
    }

    return (
        <Dialog open={true} maxWidth="lg" fullWidth={true}>
            <DialogContent>
                {model &&
                    <Survey model={model} onComplete={onComplete} />
                }
            </DialogContent>
            <DialogActions>
                {formResponseObject && <Button variant="contained" onClick={onDownloadPdf}>Download</Button>}
                <Button variant="contained" onClick={props.onClose}>Close</Button>
            </DialogActions>
        </Dialog>
    );
}

export default SurveyDialog;
