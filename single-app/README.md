# Single App

## Initialize

```shell
yarn
```

## Start

```shell
yarn start
```

## Build

```shell
yarn build
```
