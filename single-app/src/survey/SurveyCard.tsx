import { Box, Button } from "@material-ui/core"
import React from "react"
import SurveyAnalyticsDialog from "./SurveyAnalyticsDialog"
import SurveyEditorDialog from "./SurveyCreatorDialog"
import SurveyDialog from "./SurveyDialog"

interface IProps {
    surveyModel: any;
    data: any[];
}

const SurveyCard: React.FunctionComponent<IProps> = (props) => {
    const [survey, setSurvey] = React.useState<boolean>(false);
    const [creator, setCreator] = React.useState<boolean>(false);
    const [analytics, setAnalytics] = React.useState<boolean>(false);

    const onOpenSurvey = () => {
        setSurvey(true);
    }

    const onCloseSurvey = () => {
        setSurvey(false);
    }

    const onOpenCreator = () => {
        setCreator(true);
    }

    const onCloseCreator = () => {
        setCreator(false);
    }

    const onOpenAnalytics = () => {
        setAnalytics(true);
    }

    const onCloseAnalytics = () => {
        setAnalytics(false);
    }

    return (
        <Box>
            <Button variant="contained" onClick={onOpenSurvey}>Open Survey</Button>
            <Button variant="contained" onClick={onOpenCreator}>Open Creator</Button>
            <Button variant="contained" onClick={onOpenAnalytics}>Open Analytics</Button>
            {survey && <SurveyDialog surveyModel={props.surveyModel} onClose={onCloseSurvey} />}
            {creator && <SurveyEditorDialog surveyModel={props.surveyModel} onClose={onCloseCreator} />}
            {analytics && <SurveyAnalyticsDialog surveyModel={props.surveyModel} data={props.data} onClose={onCloseAnalytics} />}
        </Box>
    );
}

export default SurveyCard;
