import React from "react"
import "survey-creator/survey-creator.css"

interface IProps {
    surveyModel: any;
}

const SurveyCreatorDiv: React.FunctionComponent<IProps> = (props) => {

    React.useEffect(() => {
        import("survey-creator")
            .then(({ StylesManager, SurveyCreator }) => {
                StylesManager.applyTheme("modern");
                const options = { showLogicTab: true, showTranslationTab: true, designerHeight: 700 };
                const creator = new SurveyCreator("creatorDiv", options);
                creator.isAutoSave = false;
                creator.showToolbox = "right";
                creator.showPropertyGrid = "right";
                creator.text = JSON.stringify(props.surveyModel);
                creator.rightContainerActiveItem("toolbox");
            })
            .catch((error) => {
                console.error(error);
            });
    }, [props.surveyModel]);

    return <div id="creatorDiv" />;
}

export default SurveyCreatorDiv;
