import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    Box,
    TextField
} from "@material-ui/core"
import React from 'react';

interface IProps {
    onClose: () => void;
}

const DemoDialog: React.FunctionComponent<IProps> = (props) => {
    const [userInput, setUserInput] = React.useState('');

    const handleChange = (e: any) => {
        setUserInput(e.target.value);
    };

    const onClick = () => {
        // Vulnerable: User input is used to create a regular expression
        const regExp = RegExp(userInput);
        regExp.test('');
    };

    return (
        <Dialog open={true} maxWidth="lg" fullWidth={true}>
            <DialogContent>
                <TextField variant="outlined" value={userInput} onChange={handleChange} />
                <Button variant="contained" color="primary" onClick={onClick}>Search</Button>
                {/* Vulnerable: User input is rendered as HTML */}
                <Box dangerouslySetInnerHTML={{ __html: userInput }}></Box>
            </DialogContent>
            <DialogActions>
                <Button variant="contained" onClick={props.onClose}>Close</Button>
            </DialogActions>
        </Dialog>
    );
}

export default DemoDialog;