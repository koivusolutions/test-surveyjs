import React from "react"
import SurveyCard from "./survey/SurveyCard"

const surveyModel = {
    "pages": [
        {
            "name": "page1",
            "elements": [
                {
                    "type": "checkbox",
                    "name": "singleApp",
                    "title": "Single App Question",
                    "choices": [
                        "Single A",
                        "Single B",
                        "Single C"
                    ]
                }
            ]
        }
    ]
};

const data: any[] = [
    {
        "singleApp": [
            "Single A"
        ]
    },
    {
        "singleApp": [
            "Single A"
        ]
    },
    {
        "singleApp": [
            "Single B"
        ]
    },
];

const App: React.FunctionComponent = () => {
    return <SurveyCard surveyModel={surveyModel} data={data} />
}

export default App;
